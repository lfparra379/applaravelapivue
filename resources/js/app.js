require('./bootstrap');

window.Vue = require('vue');

//Vue.component('example-component', require('./components/ExampleComponents.vue').default);
Vue.component('category-main-component', require('./categories/components/CategoryMain.vue').default);

Vue.component('movie-main-component', require('./movies/components/MovieMain.vue').default);

Vue.component('user-main-component', require('./users/components/UserMain.vue').default);

Vue.component('rental-main-component', require('./rental/components/RentalMain.vue').default);

Vue.component('rol-main-component', require('./rol/components/RolMain.vue').default);

Vue.component('status-main-component', require('./status/components/StatusMain.vue').default);

Vue.component('typestatus-main-component', require('./typestatus/components/TypestatusMain.vue').default);

new Vue({
    el: "#app"
});