<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';
    protected $fillable = ['name', 'type_status_id'];
    protected $guarded = ['id'];  
    
    //Relación muchos a uno Status a TypeStatus
    //Cuando la relacion es de muchos a uno, la funcion se coloca en singular
    //Muchos a uno belongsTo

    public function typeSatatus(){
        return $this->belongsTo('App\Models\TypeStatus');
    }

    //Relacion uno a muchos de Status y Category
    //Cuando la relacion es de uno a muchos la funcion se coloca en plural
    //uno a muchos hasMany
    public function categories(){
        return $this->hasMany('App\Models\Category');
    }

    //Relación uno a muchos de Status y Movie
    public function movies(){
        return $this->hasMany('App\Models\Movie');
    }

    //Relacion uno a uchos de Status y User
    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
