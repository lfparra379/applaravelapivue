<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name', 'status_id'];
    protected $guarded = ['id'];

    //Relación muchos a uno de Rol y Status
    public function status(){
        return $this->belongsTo('App\Models\Status');
    }

    //Relacion uno a muchos de Rol y User
    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
