<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories'; // A que tabla apunto
    protected $fillable = ['name','status_id']; // Que atributos tiene
    protected $guarded = ['id']; //Atributo protegido que es la llave primaria

    //Relacion muchos a uno de Category y Status
    public function status(){
        return $this->belongsTo('App\Models\Status');
    }

    //Relacion muchos a muchos de Category y Movie
    //Cuando la relacion es muchos a muchos es belongsToMany
    public function movies(){
        return $this->belongsToMany('App\Models\Movie');
    }


}
