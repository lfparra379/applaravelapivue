<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Crear todas las rutas de una vez
//Le estamos diciendo que nos cree todas las rutas del recurso de categorias de este controller
Route::resource('categories', 'CategoryController');

Route::resource('movies', 'MovieController'); 

Route::resource('users', 'UserController');

Route::resource('rentals', 'RentalController');

Route::resource('roles', 'RolController');

Route::resource('statuses', 'StatusController');

Route::resource('estats', 'TypeStatusController');

